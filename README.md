Introduction
===========================

The repository contains the MATLAB scripts and data for diver tracking analysis. The data was acquired during 
experiments in the the Split degaussing station in June/July 2014. 

Experiment description
---------------------------
Performing real–life experiments that include humans and robots is always a complex task. 
The unpredictable human nature does not allow replicability of experiment which is why
careful planning and preparation is always required. In order to validate diver tracking 
experiments, and replicate them under different environmental conditions, we define a benchmark
scenario that includes tracking a predefined, georeferenced underwater transect. A 50 m of rope 
was laid on seabed at the test site, and georeferenced using GPS/USBL/Video measurement. During 
the experiments, the diver was required to follow the transect in both directions (up and down), 
with the instructions to deviate as little as possible. While the diver was tracking the transect, 
the autonomous surface vehicle was tracking the diver using acoustic positioning sensor.

![Georeferenced transect](https://bytebucket.org/labust/diver-tracking/raw/master/img/geo_transect.png)


Vehicles and sensors
---------------------------

The tracking experiments were carried out with the autonomous surface vehicle PlaDyPos, 
shown in figure, that has 4 thrusters in the ”X” configuration allowing omnidirectional 
motion, i.e. motion in the horizontal plane under any orientation. PlaDyPos has been 
developed at LABUST and is 0.35m high, 0.707m wide and long, and it weighs approximately 
25kg.

![PlaDyPos](http://www.fer.unizg.hr/images/50016236/pladypos_mini_low.jpg)

The acoustic localization sensor is the Tritech MicroNav Ultra Short Baseline (USBL). 
It is used to determine the position of the diver relative to the vehicle and allow
simultaneous communication with the diver acoustic modem. While diver localization is 
the main topic of interest, it should be mentioned that the acoustic link can be 
used to transmit messages as well as diver position based on USBL and GPS
measurements from PlaDyPos.

For diver simulation the VideoRay ROV was used.

Results
---------------------------
This section will presents the results obtained during the experiment. The figures are already present in the
repository, but can be regenerated using the `matlab_scripts/generate_all.m` script. Be aware that the script 
will try to download the needed data in order to regenerate the plots. More information on the data processing
behind the results is available [**here**](https://bitbucket.org/labust/diver-tracking/wiki/Home).  

In order to perform structured experiments with the diver, a step-by-step experimental plan is designed in order to examine
the influence of different uncertainties:

* **Setup 1**: Virtual diver (VD) & PlaDyPos 

 Platform placed in a real environment tracks the virtual diver that is simulated using a simple mathematical 
 model given with (5). While this experimental setup eliminates acoustic sensor and diver
 related uncertainties, it also allows reliable testing of PlaDyPos behaviour under different measurement
 update rates and performance evaluation of the diver estimator onboard PlaDyPos in real environmental
 conditions.

* **Setup 2**: ROV & PlaDyPos

 In this real environment setup, the human diver is replaced with a remotely operated vehicle (ROV)
 with an acoustic modem pinging the USBL on the PlaDyPos, thus introducing the real acoustic channel
 uncertainties but eliminating those caused by the diver. This setup is designed to identify potential deterioration
 in system performance due to the acoustic channel characteristics.

* **Setup 3**: Diver & PlaDyPos

 The final experimental setup, which is in fact the demonstrator of the final goal of the described
 robotic system, includes experiments in real conditions with all the above mention uncertainties included.


![Setup1_NE](https://bytebucket.org/labust/diver-tracking/raw/master/figures/Figure3_1_NE_virtual.png) | ![Setup2_NE](https://bytebucket.org/labust/diver-tracking/raw/master/figures/Figure2_1_NE_ROV.png) | ![Setup3_NE](https://bytebucket.org/labust/diver-tracking/raw/master/figures/Figure1_1_NE_diver.png)
:------------------ | :---------------------- | :----------------------
   **(a)** Setup-1 North-East plot    |        **(b)** Setup-2 North-East plot     |    **(c)** Setup-3 North-East plot


![Setup1_NE_time](https://bytebucket.org/labust/diver-tracking/raw/master/figures/Figure3_2_NE_time_virtual.png) | ![Setup2_NE_time](https://bytebucket.org/labust/diver-tracking/raw/master/figures/Figure2_2_NE_time_ROV.png) | ![Setup3_NE](https://bytebucket.org/labust/diver-tracking/raw/master/figures/Figure1_2a_NE_time_diver.png)
:------------------ | :---------------------- | :----------------------
   **(a)** Setup-1 North-East time plot    |        **(b)** Setup-2 North-East time plot  |  **(c)** Setup-3 North-East time plot


![Setup1_dist](https://bytebucket.org/labust/diver-tracking/raw/master/figures/Figure3_3_distance_virtual.png) | ![Setup2_dist](https://bytebucket.org/labust/diver-tracking/raw/master/figures/Figure2_3_distance_ROV.png) | ![Setup3_NE](https://bytebucket.org/labust/diver-tracking/raw/master/figures/Figure5_a_tracking_error_diver.png)
:------------------ | :---------------------- | :----------------------
   **(a)** Setup-1 tracking error    |        **(b)** Setup-2 tracking error | **(c)** Setup-3 tracking error

![BoxPlot](https://bytebucket.org/labust/diver-tracking/raw/master/figures/Figure6_a_measurement_error_box.png) |
:------------------ |
   **(a)** Box-plot of the measured tracking error    |