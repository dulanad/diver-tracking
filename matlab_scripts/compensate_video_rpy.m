function vdata = compensate_video_rpy(vdata, pdata, imures)
    vdata.diver_relative_video.n = zeros(size(vdata.diver_relative_video.x));
    vdata.diver_relative_video.e = zeros(size(vdata.diver_relative_video.x));
    vdata.diver_relative_video.yaw = zeros(size(vdata.diver_relative_video.x));
    vdata.diver_relative_video.roll = zeros(size(vdata.diver_relative_video.x));
    vdata.diver_relative_video.pitch = zeros(size(vdata.diver_relative_video.x));
    
    offsetx = 0.35;
    orientation = imures.orientation;
    usblz = -pdata.usblusbl_nav.point(3,:) + 0.6;
    if (isfield(vdata,'uidx'))
        orientation = orientation(:,vdata.uidx);
    end
           
    for j=1:length(vdata.diver_relative_video.x)
        z = usblz(j);
        if abs(z) >= 7
            z = 6;
        end
        
        if abs(z) <= 3
            z = 4;
        end
                
        %z = 100;
        
        or = orientation(:,j);    
        [y,p,r] = quat2angle([or(4) or(1) or(2) or(3)], 'ZYX');
        xyzd = euler_rotation([0,0,z],-[r,p,0]);
        xy = [-vdata.diver_relative_video.y(j) + offsetx, vdata.diver_relative_video.x(j),xyzd(3)]';
        ne = euler_rotation(xy', [r,p,y]);
        vdata.diver_relative_video.n(j) = ne(1);
        vdata.diver_relative_video.e(j) = ne(2); 
        vdata.diver_relative_video.yaw(j) = y;
        vdata.diver_relative_video.pitch(j) = p;
        vdata.diver_relative_video.roll(j) = r;       
 
    end 
end