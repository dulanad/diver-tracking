function table = diver_plot(directory)
%close all;
%% Diver 10.55 test scripts
% Works on 8 tracks from the 10.55 log.
% Track 1 
%   - approach after 84s
%   - has video with visible diver
%   - no manual intervention
% Track 2 
%   - diver lost on start (bad usbl)
%   - continues tracking when usbl measurements are available
%   - manual intervention (>644s)
%   - has video of diver
% Track 3
%   - diver is still lost from Track2
%   - diver found 770s-936s (try to catch on, signal with losses)
%   - diver lost (bad usbl)
%   - couple of signals at 992s-end
%   - exclude Track3 from processing
%   - has some video of diver
% Track 4
%   - diver diverges from transect around 1250
%   - diver lost after > 1256 
%   - continued tracking after > 1272
% Track 5
%   - track is ok with full time tracking
%   - full-time diver in video
% Track 6
%   - usbl signal lost after first seconds
%   - continued tracking without interruption after new signals
%   - diver lost > 1665s
%   - diver lost in video image
%   - catches up with the diver at the end
%   - manual interventions [1665, 1676], [1770,1793]
%   - turned off > 1903.11
% Track 7
%   - resumed tracking > 2038.43
%   - has video of diver
% Track 8
%   - diver in video
%   - directly continues tracking from Track 7
%   - heading changes for different video angles

figdir = [directory,'figures'];

%% Load the data from the log
load([directory, '/data/diver_data/topside_2014_07_04_10_55_30_filtered.mat']);
load([directory, '/data/diver_data/video_2014_07_04_10_55_30_resampled.mat']);
load([directory, '/data/diver_data/topside_2014_07_04_10_55_30_filtered_imu.mat']);

% Correct video scale (from a 9.3mm estimate to real 8mm based on measuring the ROV across frames)
videodatares.diver_relative_video.x = videodatares.diver_relative_video.x*0.008/0.0093;
videodatares.diver_relative_video.y = videodatares.diver_relative_video.y*0.008/0.0093;

t12idx = find((tres >= 84) & (tres <= 644));
t3idx = find((tres >= 770) & (tres <= 936));
t34idx = find((tres >= 992) & (tres <=1240));
t456idx = find((tres >= 1272) & (tres <= 1665));
t6idx = find((tres >= 1676) & (tres <= 1770));
t6bidx = find((tres>=1793) & (tres <= 1903.11));
t7idx = find((tres >= 2038.43) & (tres <= 2531));

% Create a time union above working tracking
tidx1 = union(t12idx, t456idx);
tidx2 = union(t6idx,t6bidx);
tidx3 = union(t3idx, t34idx);
tidx = union(tidx1, tidx2);
tidx = union(tidx, t7idx);
tidx = union(tidx,tidx3);

tdata = subsample_struct(datares, tidx);
tt = tres(tidx);
idata = subsample_struct(imures, tidx);
vdata = subsample_struct(videodatares, tidx);
% Process data
vdata = compensate_video_rpy(vdata, tdata, idata);
vdata = extract_video_meas(tt, vdata);
dxv = vdata.diver_relative_video.n;
dyv = vdata.diver_relative_video.e;
% Get the track statistics
table = track_stats(tt, tdata);
d = sqrt(dxv.^2 + dyv.^2);
table.vid.track_mean = mean(d(~isnan(d)));
table.vid.data = d;
% Prepare data
tusblm = extract_usbl_abs(tt, tdata);

%% North-east plot
hfig = figure;
hold on;
title('N-E plot');

plot(tusblm.y, tusblm.x, 'rx');
plot(tdata.TrackPoint.position(2,:), tdata.TrackPoint.position(1,:),'b','LineWidth',2);


axis(gca,[-25,35,5,65]);
xlabel('East [m]');
ylabel('North[m]');
  
legend('Diver position measurement', 'Diver position estimate', ... 
    'Location', 'SouthEast');

setup_plot(hfig);
save_plot('Figure1_1_NE_diver',figdir);

%% Time plot 1a
hfig = figure;
% Extract leg
pdata = subsample_struct(datares, t12idx);
ptt = tres(t12idx);
pusblm = extract_usbl_abs(ptt, pdata);
% Normalize time
pusblm.pt = pusblm.t - ptt(1);
ptt = ptt - ptt(1);
% Plot data
plot_ne_single(ptt,pdata,pusblm);
% Setup plot settings
subplot(2,1,1);
axis(gca,[0,550,15,65]);
title('North plot');
xlabel('Time [s]');
ylabel('North [m]');
legend('Diver measured','Diver estimated', ...
        'ASV{\it PlaDyPos} estimated','Location','South'); 
    
subplot(2,1,2);
axis(gca,[0,550,-21,35]);
title('East plot');
xlabel('Time [s]');
ylabel('East [m]');

setup_plot(hfig);
save_plot('Figure1_2a_NE_time_diver',figdir);
 
%% Time plot 2
hfig = figure;
% Extract leg
pdata = subsample_struct(datares, t7idx);
ptt = tres(t7idx);
pusblm = extract_usbl_abs(ptt, pdata);
% Normalize time
pusblm.pt = pusblm.t - ptt(1);
ptt = ptt - ptt(1);
% Plot data
plot_ne_single(ptt,pdata,pusblm);

% Setup plot 
subplot(2,1,1);
axis(gca,[0,490,15,65]);
title('North plot');
xlabel('Time [s]');
ylabel('North [m]');
legend('Diver measured','Diver estimated', ...
        'ASV{\it PlaDyPos} estimated','Location','South'); 
    
subplot(2,1,2);
axis(gca,[0,490,-21,35]);
title('East plot');
xlabel('Time [s]');
ylabel('East [m]');

setup_plot(hfig);
save_plot('Figure1_2b_NE_time_diver',figdir);

%% Time plot 3
hfig = figure;
% Extract leg
pdata = subsample_struct(datares, t12idx);
vdata = subsample_struct(videodatares, t12idx);
idata = subsample_struct(imures, t12idx);
ptt = tres(t12idx);
% Process data
vdata = compensate_video_rpy(vdata, pdata, idata);
% Subsample measurements
pusblm = extract_usbl_abs(ptt, pdata);
vdata = extract_video_meas(ptt, vdata);
vdata.diver_relative_video
% Normalize time
pusblm.t = pusblm.t - ptt(1);
vdata.tt = vdata.tt - ptt(1);
ptt = ptt - ptt(1);
% Plot
[md1, md2, md3] = plot_distance_single(ptt,pdata,pusblm,vdata);
% Setup plot
title('Tracking Error');
axis(gca,[0,550,0,4]);
set(gca, 'YTick', [0, md2, md1, 2:2:12]);
xlabel('Time [s]');
ylabel('Tracking Error [m]');

setup_plot(hfig);
save_plot('Figure5_a_tracking_error_diver',figdir);

%% Time plot 4
hfig = figure;
% Extract leg
pdata = subsample_struct(datares, t7idx);
vdata = subsample_struct(videodatares, t7idx);
idata = subsample_struct(imures, t7idx);
ptt = tres(t7idx);
% Process data
vdata = compensate_video_rpy(vdata, pdata, idata);
% Subsample measurements
pusblm = extract_usbl_abs(ptt, pdata);
vdata = extract_video_meas(ptt, vdata);
% Normalize time
pusblm.t = pusblm.t - ptt(1);
vdata.tt = vdata.tt - ptt(1);
ptt = ptt - ptt(1);
% Plot
[md1, md2, md3] = plot_distance_single(ptt,pdata,pusblm,vdata);
% Setup plot
title('Tracking Error');
axis(gca,[0,490,0,6]);
set(gca, 'YTick', [0, md2, md1, 4:2:8]);
xlabel('Time [s]');
ylabel('Tracking Error [m]');

setup_plot(hfig);
save_plot('Figure5_b_tracking_error_diver',figdir);
end

function [md1, md2, md3] = plot_distance_single(ptt,pdata,pusblm,vdata)
% Plot
hold on;
dxm = pdata.stateHat.position(1,pusblm.uidx) - pusblm.x;
dym = pdata.stateHat.position(2,pusblm.uidx) - pusblm.y;
    
dxe = pdata.stateHat.position(1,:) - pdata.TrackPoint.position(1,:);
dye = pdata.stateHat.position(2,:) - pdata.TrackPoint.position(2,:);
     
d = sqrt(dxm.^2+dym.^2);
md1 = mean(d);
h1 = plot(pusblm.t, d,'rx'); 
h1b = plot(pusblm.t, md1*ones(size(pusblm.t)),'r--'); 
d = sqrt(dxe.^2+dye.^2);
md2 = mean(d);
h2 = stairs(ptt, d,'b','LineWidth',2);  
h2b = plot(ptt, md2*ones(size(ptt)),'b--');
d = sqrt((vdata.diver_relative_video.n).^2 + (vdata.diver_relative_video.e).^2);
md3 = mean(d(~isnan(d)));
h3 = plot(vdata.tt, d,'g.','LineWidth',2);
h3b = plot(ptt, md3*ones(size(ptt)),'g--');
legend([h1,h2,h3], 'USBL measurement', 'Diver estimate', ...
    'Video validation', 'Location','NorthEast'); 
end

