function [outvec] = euler_rotation(invec,eulerangle)

phi = eulerangle(1); theta = eulerangle(2); psi = eulerangle(3);

% Shortcuts
c1 = cos(phi);   s1 = sin(phi);
c2 = cos(theta); s2 = sin(theta); t2 = tan(theta);
c3 = cos(psi);   s3 = sin(psi);

% Transformation matrix: eta1_E_dot=J1*v1_G
J1=[(c3*c2)            (c3*s2*s1-s3*c1)          (s3*s1+c3*c1*s2); 
    (s3*c2)            (c1*c3+s1*s2*s3)          (c1*s2*s3-c3*s1); 
    -s2                (c2*s1)                   (c1*c2)];

% Transformation matrix: eta2_E_dot=J2*v2_G
J2=[1            (s1*t2)          (c1*t2); 
    0               c1              -s1; 
    0            (s1/c2)          (c1/c2)];

%outvec = [J1  zeros(3,3);zeros(3,3) J2]*invec;

outvec = [J1']*invec';

%%% invec is [u v w p q r]'
%%% outvec is [ue ve we pe qe re]'
