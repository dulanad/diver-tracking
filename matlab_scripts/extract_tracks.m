function extract_tracks(directory)

    files = {};
    files.name = {};
    files.start = {};
    files.refline={};
    files.isLatLon = {};
    files.append = {};
    
    jrefline = [43.510358, 16.390694; 43.510708, 16.391314];
    vrefline = [0,0; 30, 0];

    %% Fill in the metadata for each file
    files.name{end+1} = [directory, '/topside_2014_07_04_10_55_30_filtered'];
    files.start{end+1} = [0, 385.6, 733.6, 1024.83, 1319.31, 1630.96, 1958.4,2307.5, 2534.1];
    % T1 and T2 in latitude and longitude
    files.refline{end+1} = jrefline;
    files.isLatLon{end+1} = 1;
    files.append{end+1} = '_Diver_1Hz.mat';
    
    files.name{end+1} = [directory, '/topside_2014_07_02_15_39_00_filtered'];
    files.start{end+1} = [393.92, 561.92, 730.93, 901.91, 1070.93, 1239.92, 1412.15, 1581.14, 1749.15];
    % T1 and T2 in latitude and longitude
    files.refline{end+1} = vrefline;
    files.isLatLon{end+1} = 0;
    files.append{end+1} = '_1Hz_VarSpeed_0.005.mat';
    
    files.name{end+1} = [directory, '/topside_2014_07_02_15_39_00_filtered'];
    files.start{end+1} = [1873.34, 2044.36, 2213.35, 2381.35, 2550.38];
    % T1 and T2 in latitude and longitude
    files.refline{end+1} = vrefline;
    files.isLatLon{end+1} = 0;
    files.append{end+1} = '_1Hz_VarSpeed_0.05.mat';
    
    files.name{end+1} = [directory, '/topside_2014_07_02_15_39_00_filtered'];
    files.start{end+1} = [2611.33, 2710.35, 2810.35, 2910.35, 3010.35];
    % T1 and T2 in latitude and longitude
    files.refline{end+1} = vrefline;
    files.isLatLon{end+1} = 0;
    files.append{end+1} = '_1Hz_FixSpeed_0.005.mat';
    
    files.name{end+1} = [directory, '/topside_2014_07_02_15_39_00_filtered'];
    files.start{end+1} = [3068.55, 3167.56, 3268.56];
    % T1 and T2 in latitude and longitude
    files.refline{end+1} = vrefline;
    files.isLatLon{end+1} = 0;
    files.append{end+1} = '_1Hz_FixSpeed_0.05.mat';
    
    files.name{end+1} = [directory, '/topside_2014_07_02_15_39_00_filtered'];
    files.start{end+1} = [3304.55, 3405.26, 3505.57];
    % T1 and T2 in latitude and longitude
    files.refline{end+1} = vrefline;
    files.isLatLon{end+1} = 0;
    files.append{end+1} = '_10Hz_FixSpeed_0.005.mat';
        
    files.name{end+1} = [directory, '/topside_2014_07_02_15_39_00_filtered'];
    files.start{end+1} = [3304.55, 3405.26, 3505.57];
    % T1 and T2 in latitude and longitude
    files.refline{end+1} = vrefline;
    files.isLatLon{end+1} = 0;
    files.append{end+1} = '_10Hz_FixSpeed_0.005.mat';    
    
    files.name{end+1} = [directory, '/topside_2014_07_02_18_50_38_filtered'];
    files.start{end+1} = [161.29, 261.11, 361.11, 469.1, 569.1];
    % T1 and T2 in latitude and longitude
    files.refline{end+1} = vrefline;
    files.isLatLon{end+1} = 0;
    files.append{end+1} = '_10Hz_FixSpeed_0.05.mat';    
    
    files.name{end+1} = [directory, '/topside_2014_07_02_18_50_38_filtered'];
    files.start{end+1} = [634.3, 730.3, 831.6, 931.59, 1031.58, 1131.82, 1233.36];
    % T1 and T2 in latitude and longitude
    files.refline{end+1} = vrefline;
    files.isLatLon{end+1} = 0;
    files.append{end+1} = '_0.25Hz_FixSpeed_0.05.mat';
    
    files.name{end+1} = [directory, '/topside_2014_07_02_18_50_38_filtered'];
    files.start{end+1} = [1276.15, 1372.52, 1473.34, 1571.02, 1674.88];
    % T1 and T2 in latitude and longitude
    files.refline{end+1} = vrefline;
    files.isLatLon{end+1} = 0;
    files.append{end+1} = '_0.25Hz_FixSpeed_0.005.mat';
    
    files.name{end+1} = [directory, '/topside_2014-07-03-09-48-12_part1_filtered'];
    files.start{end+1} = [422.08, 774.7, 1192.6, 1392.48, 1700.1, 1895.5862];
    % T1 and T2 in latitude and longitude
    files.refline{end+1} = jrefline;
    files.isLatLon{end+1} = 1;
    files.append{end+1} = '_VR.mat';
    
    files.name{end+1} = [directory, '/topside_2014_07_04_12_36_00_filtered'];
    files.start{end+1} = [175, 517.5, 842.7, 1101, 1423.39];
    % T1 and T2 in latitude and longitude
    files.refline{end+1} = jrefline;
    files.isLatLon{end+1} = 1;
    files.append{end+1} = '_Diver_2.mat'; 
    
    for i=1:length(files.name)
        % load tres, datares, data
        load([files.name{i},'.mat']);
        tstart = files.start{i};
        refline = files.refline{i};
        [t, data] = split_tracks(tres,datares,tstart);
        if (files.isLatLon{i})
            % extract lat-lon origin data
            lat0 = datares.stateHat.origin(1,1);
            lon0 = datares.stateHat.origin(2,1);
            % convert to n-e
            % The custom local conversion is used that was used at the
            % time of the log. However, newer logs are created with
            % the geo->ecef->ned conversion scheme.
            [T1dx,T1dy] = latlon_to_m(refline(1,1)-lat0, refline(1,2)-lon0, lat0);
            [T2dx,T2dy] = latlon_to_m(refline(2,1)-lat0, refline(2,2)-lon0, lat0);     
            refline = [T1dx,T1dy; T2dx,T2dy];
        end
        save([files.name{i},files.append{i}],'t','data','refline');
    end
end