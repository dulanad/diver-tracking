function usblm = extract_usbl_abs(t, data)
   % Extract only usbl measurements (avoid repeated values)
   % Filter the data to show only unique points on a time-plot
   [dxm, ia1, ic1]  = unique(data.usblusbl_nav.point(1,:));
   [dym, ia2, ic2]  = unique(data.usblusbl_nav.point(2,:));
   uidx = union(ia1,ia2);
   dxm = data.usblusbl_nav.point(1,uidx);
   dym = data.usblusbl_nav.point(2,uidx);
   tu = t(uidx);
   
   
   x0 = data.stateHat.position(1,uidx);
   y0 = data.stateHat.position(2,uidx);
   
   usblm.x = x0 + dxm;
   usblm.y = y0 + dym;
   usblm.t = tu;
   usblm.uidx = uidx;
end