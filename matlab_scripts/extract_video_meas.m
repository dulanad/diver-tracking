function video = extract_video_meas(t, data)
   % Extract only usbl measurements (avoid repeated values)
   % Filter the data to show only unique points on a time-plot
   [dxm, ia1, ic1]  = unique(data.diver_relative_video.x);
   [dym, ia2, ic2]  = unique(data.diver_relative_video.y);
   uidx = union(ia1,ia2);
   dxm = data.diver_relative_video.x(uidx);
   dym = data.diver_relative_video.y(uidx);
   tu = t(uidx);
  
   video.diver_relative_video.x = dxm;
   video.diver_relative_video.y = dym;
   data.diver_relative_video
   if isfield(data.diver_relative_video, 'n')
      video.diver_relative_video.n = data.diver_relative_video.n(uidx);
      video.diver_relative_video.e = data.diver_relative_video.e(uidx);
   end
   video.diver_relative_video.y = dym;
   video.diver_relative_video.yaw = data.diver_relative_video.yaw(uidx);
   video.diver_relative_video.pitch = data.diver_relative_video.pitch(uidx);
   video.diver_relative_video.roll = data.diver_relative_video.roll(uidx);
   video.tt = tu;
   video.uidx = uidx;
end