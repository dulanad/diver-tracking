function tables = generate_all()
close all;
%% Download data if needed
filename = mfilename('fullpath');
[directory,name,ext] = fileparts(filename);
basedir = [directory, '/../'];
if ~exist([basedir,'data'],'dir')
    disp('Could not find "data" directory. Trying to download.');
    
    % Create the directory where to download raw_data
    mkdir(directory,'/../data');
    %Get the virtual diver data
    data = {'rov_data.zip', 'diver_data.zip', 'virtual_data.zip'};
    urlbase = 'https://bitbucket.org/labust/diver-tracking/downloads/';
    for i=1:length(data)
        [filestr, status] = urlwrite([urlbase, data{i}],...
            [basedir, '/data/',data{i}]);
        if status == 1
            disp(['Downloaded ',data{i},'. Unzipping...']);
            unzip(filestr, [basedir,'/data/']);  
            disp(['Unzipped.']);
        else
            disp(['Failed to download ',data{i},'.']);
        end
    end
end

% Check for figures directory
if ~exist([basedir,'figures'], 'dir');
    mkdir(basedir,'figures');
end

%% Generate the speed plot
speed_plot(basedir);

%% Generate the virtual diver plots
tables.virtual = virt_plot(basedir);

%% Generate the videoray plots
tables.videoray = vr_plot(basedir);

%% Generate the diver plots
tables.diver = diver_plot(basedir);

%% Generate tables
f = fopen([basedir,'latex_tabular1.txt'],'w');
fprintf(f, '\\begin{tabular}{| l | c | c |} \\hline \n');
fprintf(f, '& Measurement variance & Estimate variance \\\\ \\hline \n');
fprintf(f, 'Setup 2 & %.4f & %.4f \\\\ \n', tables.virtual.meas.fit_var, tables.virtual.est.fit_var); 
fprintf(f, 'Setup 3 & %.4f & %.4f \\\\ \n', tables.videoray.meas.fit_var, tables.videoray.est.fit_var); 
fprintf(f, 'Setup 4 & %.4f & %.4f \\\\ \\hline \n', tables.diver.meas.fit_var, tables.diver.est.fit_var); 
fprintf(f, '\\end{tabular} \n');
fclose(f);

f = fopen([basedir,'latex_tabular2.txt'],'w');
fprintf(f, '\\begin{tabular}{| l | c | c | c |} \\hline \n');
fprintf(f, '& Measured tracking error & Estimated tracking error & Validation tracking error \\\\ \\hline \n');
fprintf(f, 'Setup 2 & %.4f & %.4f & N/A \\\\ \n', tables.virtual.meas.track_mean, tables.virtual.est.track_mean); 
fprintf(f, 'Setup 3 & %.4f & %.4f & %.4f \\\\ \n', tables.videoray.meas.track_mean, tables.videoray.est.track_mean, tables.videoray.vid.track_mean); 
fprintf(f, 'Setup 4 & %.4f & %.4f & %.4f \\\\ \\hline \n', tables.diver.meas.track_mean, tables.diver.est.track_mean, tables.diver.vid.track_mean); 
fprintf(f, '\\end{tabular} \n');
fclose(f);

%% Generate measurement box-plot
boxdata = [tables.virtual.meas.data,tables.videoray.meas.data,tables.diver.meas.data];
md1 = median(tables.virtual.meas.data);
md2 = median(tables.videoray.meas.data);
md3 = median(tables.diver.meas.data);
gvirt = repmat({['Setup 1 (M = ',num2str(md1,2),' m)']},1,length(tables.virtual.meas.data));
gvr = repmat({['Setup 2 (M = ',num2str(md2,2),' m)']},1,length(tables.videoray.meas.data));
gdiver = repmat({['Setup 3 (M = ',num2str(md3,2),' m)']}, 1, length(tables.diver.meas.data));

hfig = figure;
bh = boxplot(boxdata,[gvirt,gvr,gdiver]);

% Setup plot
title('USBL measured tracking error distribution');
axis(gca,[0.5,3.5,0,8]);
set(gca, 'YTick', [0:2:8]);
%set(gca, 'YAxisLocation', 'right', 'YTick', [md1, md3]);
%xlabel('Agent');
ylabel('Error [m]');

setup_plot(hfig);

% Setup line widths
for i=1:size(bh,1)-1
     set(bh(i,1),'linewidth',3);
     set(bh(i,2),'linewidth',3);
     set(bh(i,3),'linewidth',3);
end

ht = findobj(gca,'Type','text');
for i=1:length(ht)
    sf=get(ht(i), 'Position');
    sf(2)=sf(2) - 30;
    set(ht(i), 'Position', sf);
end

% Bug fix
ax = findobj(gcf,'type','axes');
for j=1:length(ax),
  boxparent = getappdata(ax(j),'boxplothandle');
  listeners = getappdata(boxparent,'boxlisteners');
  for i = 1:length(listeners),
    listeners{i}.Enabled = 'off';
  end
end

save_plot('Figure6_a_measurement_error_box',[basedir,'/figures']);

%% Generate estimate box-plot
boxdata = [tables.virtual.est.data,tables.videoray.est.data,tables.diver.est.data];
md1 = median(tables.virtual.est.data);
md2 = median(tables.videoray.est.data);
md3 = median(tables.diver.est.data);
gvirt = repmat({['Setup 1 (M = ',num2str(md1,2),' m)']},1,length(tables.virtual.est.data));
gvr = repmat({['Setup 2 (M = ',num2str(md2,2),' m)']},1,length(tables.videoray.est.data));
gdiver = repmat({['Setup 3 (M = ',num2str(md3,2),' m)']}, 1, length(tables.diver.est.data));

hfig = figure;
bh = boxplot(boxdata,[gvirt,gvr,gdiver]);

% Setup plot
title('Estimated tracking error distribution');
axis(gca,[0.5,3.5,0,6]);
set(gca, 'YTick', [0:2:8]);
%set(gca, 'YAxisLocation', 'right', 'YTick', [md1, md3]);
%xlabel('Agent');
ylabel('Error [m]');

setup_plot(hfig);

% Setup line widths
for i=1:size(bh,1)-1
     set(bh(i,1),'linewidth',3);
     set(bh(i,2),'linewidth',3);
     set(bh(i,3),'linewidth',3);
end

ht = findobj(gca,'Type','text');
for i=1:length(ht)
    sf=get(ht(i), 'Position');
    sf(2)=sf(2) - 30;
    set(ht(i), 'Position', sf);
end

% Bug fix
ax = findobj(gcf,'type','axes');
for j=1:length(ax),
  boxparent = getappdata(ax(j),'boxplothandle');
  listeners = getappdata(boxparent,'boxlisteners');
  for i = 1:length(listeners),
    listeners{i}.Enabled = 'off';
  end
end

save_plot('Figure6_b_estimate_error_box',[basedir,'/figures']);

%% Generate video box-plot
boxdata = [tables.videoray.vid.data,tables.diver.vid.data];
md2 = median(tables.videoray.vid.data(~isnan(tables.videoray.vid.data)));
md3 = median(tables.diver.vid.data(~isnan(tables.diver.vid.data)));
gvr = repmat({['Setup 2 (M = ',num2str(md2,2),' m)']},1,length(tables.videoray.vid.data));
gdiver = repmat({['Setup 3 (M = ',num2str(md3,2),' m)']}, 1, length(tables.diver.vid.data));

hfig = figure;
bh = boxplot(boxdata,[gvr,gdiver]);

% Setup plot
title('Video measured tracking error distribution');
axis(gca,[0.5,2.5,0,4]);
set(gca, 'YTick', [0:2:8]);
%set(gca, 'YAxisLocation', 'right', 'YTick', [md1, md3]);
%xlabel('Agent');
ylabel('Error [m]');

setup_plot(hfig);

% Setup line widths
for i=1:size(bh,1)-1
     set(bh(i,1),'linewidth',3);
     set(bh(i,2),'linewidth',3);
end

ht = findobj(gca,'Type','text');
for i=1:length(ht)
    sf=get(ht(i), 'Position');
    sf(2)=sf(2) - 30;
    set(ht(i), 'Position', sf);
end

% Bug fix
ax = findobj(gcf,'type','axes');
for j=1:length(ax),
  boxparent = getappdata(ax(j),'boxplothandle');
  listeners = getappdata(boxparent,'boxlisteners');
  for i = 1:length(listeners),
    listeners{i}.Enabled = 'off';
  end
end

save_plot('Figure6_c_video_error_box',[basedir,'/figures']);

end

