function [dx, dy] = latlon_to_m(dlat,dlon, alat)
%  [dx, dy] = latlon_to_m(dlat,dlon,alat)
% dx   = latitude difference in meters
% dy   = longitude difference in meters
% dlat = latitude difference in degrees
% dlon = longitude difference in degrees
% alat = average latitude between the two fixes
% Reference: American Practical Navigator, Vol II, 1975 Edition, p 5 

rlat = alat * pi/180;
m = 111132.09 * ones(size(rlat)) - ...
    566.05 * cos(2 * rlat) + 1.2 * cos(4 * rlat);
p = 111415.13 * cos(rlat) - 94.55 * cos(3 * rlat);

dx = dlat .* m;
dy = dlon .* p;
end