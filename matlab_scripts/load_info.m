function bags = load_info(info)
    % Import the XPath classes
    import javax.xml.xpath.*

    %% Load the info file
    try
        tree = xmlread(info);
    catch
        error('Failed to read XML file %s.',info);
    end


    % Create an XPath expression.
    factory = XPathFactory.newInstance;
    xpath = factory.newXPath;

    expression = xpath.compile('filter_info');
    root = expression.evaluate(tree,XPathConstants.NODE);
    % Get the topic groups
    groups = parse_groups(root);
    
    expression = xpath.compile('bag');
    bagList = expression.evaluate(root,XPathConstants.NODESET);
    
    bags = {};
    for i=1:bagList.getLength
        bnode = bagList.item(i-1);
        name = char(bnode.getAttribute('file'));

        if (length(name) > 3) && strcmp(name(end-3:end),'.bag')
            name = name(1:end-4);
        end
                
        % Find
        outname = char(bnode.getAttribute('out'));
        if strcmp(outname, '')
            outname = [name, '_filtered.bag'];
        end

        bagFile = struct;
        bagFile.name = outname;
        bagFile.filename = [name, '.bag'];
        bagFile.topics = get_topics(bnode);

        % Find any groups
        expression = xpath.compile('tgroup');
        gList = expression.evaluate(bnode,XPathConstants.NODESET);
        for j=1:gList.getLength
            gnode = gList.item(j-1);
            gname = char(gnode.getAttribute('name'));
            if groups.isKey(gname)
                ttemp =  groups(gname);
                bagFile.topics.names = [bagFile.topics.names, ttemp.names];
                bagFile.topics.umeta = [bagFile.topics.umeta, ttemp.umeta];
            end
        end

        bags{end+1} = bagFile;
    end
    return
end

function groups = parse_groups(root)
    % Import the XPath classes
    import javax.xml.xpath.*

    factory = XPathFactory.newInstance;
    xpath = factory.newXPath;
    expression = xpath.compile('/filter_info/tgroup');
    gList = expression.evaluate(root,XPathConstants.NODESET);

    groups = containers.Map();
    for i=1:gList.getLength
        gnode = gList.item(i-1);
        name = char(gnode.getAttribute('name'));
        if strcmp(name,'') == 0
            groups(name) = get_topics(gnode);
        end
    end
end

function topics = get_topics(node)
    import javax.xml.xpath.*

    factory = XPathFactory.newInstance;
    xpath = factory.newXPath;
    expression = xpath.compile('./topic');
    tList = expression.evaluate(node,XPathConstants.NODESET);

    topics = struct;
    topics.names = {};
    topics.umeta = [];

    for i=1:tList.getLength
        tnode = tList.item(i-1);
        name = char(tnode.getAttribute('name'));
        if strcmp(name,'') == 0
            topics.names{end+1}=name;
            umeta = char(tnode.getAttribute('meta_time'));
            flag = strcmpi(umeta, 'TRUE') || strcmp(umeta,'1');
            topics.umeta = [topics.umeta flag];
        end
    end
end

