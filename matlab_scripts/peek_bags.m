function peek_bags(directory)

files = dir([directory, '/*.bag*']);
for i=1:length(files)
    close all;
    try
    bag = ros.Bag([directory,'/',files(i).name]);
    catch
        disp(['Unindex bag file: ',files(i).name]);
    end
    [data, meta] = export_bag(bag,{'/stateHat'},1);
    
    figure('name',files(i).name)
    subplot(2,1,1);
    plot(data.stateHat.header.stamp.time, data.stateHat.position(1,:));
    title('North');
    
    subplot(2,1,2);
    plot(data.stateHat.header.stamp.time, data.stateHat.position(2,:));
    title('East');
    
    pause;
end

end