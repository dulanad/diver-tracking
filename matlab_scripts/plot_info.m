function [figsize, fontsize, legsize] = plot_info()
%% Predefined plot characteristics
scrsz = get(0,'ScreenSize');
figsize = [1 scrsz(4)/2 scrsz(3)/2 scrsz(4)/2];
fontsize = 18;
legsize = 7*fontsize/9;
end

