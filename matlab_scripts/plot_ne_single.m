function plot_ne_single(ptt, pdata, ptusblm)
    subplot(2,1,1);
    hold on;
    plot(ptusblm.pt, ptusblm.x,'rx'); 
    plot(ptt, pdata.TrackPoint.position(1,:),'b','LineWidth',2);
    plot(ptt, pdata.stateHat.position(1,:),'g','LineWidth',1.5);
      
    subplot(2,1,2);
    hold on;   
    plot(ptusblm.pt, ptusblm.y,'rx'); 
    plot(ptt, pdata.TrackPoint.position(2,:),'b','LineWidth',2);
    plot(ptt, pdata.stateHat.position(2,:),'g','LineWidth',1.5);
end