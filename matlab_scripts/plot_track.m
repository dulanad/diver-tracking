function dist = plot_track(t, data, refline)

virtualFlag = 0;
usblxm = [];
usblym = [];
usblt = [];

if isfield(data,'usblTrackPoint_Gen')
    virtualFlag = 1;
    [usblxm, ia1, ic1]  = unique(data.usblTrackPoint_Gen.position(1,:));
    [usblym, ia2, ic2]  = unique(data.usblTrackPoint_Gen.position(2,:));
    uidx = union(ia1,ia2);
    usblxm = data.usblTrackPoint_Gen.position(1,uidx);
    usblym = data.usblTrackPoint_Gen.position(2,uidx);
    usblt = t(uidx);
end


 %% North-east space plot
 hfig = figure;
 hold on;
 title('N-E plot');

 plot(data.stateHat.position(2,:), data.stateHat.position(1,:),'b');
 plot(data.TrackPoint.position(2,:), data.TrackPoint.position(1,:),'g');
 
 if virtualFlag == 1
     plot(usblym, usblxm, 'rx', 'LineWidth',2);
     legend('PlaDyPos','Diver estimate','Simulated measurements');
 else
     plot(refline(:,2),refline(:,1),'k','LineWidth',2);
     legend('PlaDyPos','Diver estimate','Transect');
 end
  
 setup_plot(hfig);
 
 %% North, east time plot
 hfig = figure;
 subplot(2,1,1);
 title('North plot');
 hold on;
 
 stairs(t, data.stateHat.position(1,:));
 plot(t, data.TrackPoint.position(1,:),'g');
 if virtualFlag == 1
     plot(usblt, usblxm, 'rx', 'LineWidth',2);
     legend('PlaDyPos','Diver estimate','Simulated measurements');
 else
     plot([t(1),t(end)],refline(:,1),'k','LineWidth',2);
     legend('PlaDyPos','Diver estimate','Transect');
 end
 
 legend('PlaDyPos','Diver');
 
 
 subplot(2,1,2);
 title('East plot');
 hold on;
 
 stairs(t, data.stateHat.position(2,:));
 plot(t, data.TrackPoint.position(2,:),'g');
 if virtualFlag == 1
     plot(usblt, usblym, 'rx', 'LineWidth',2);
     legend('PlaDyPos','Diver estimate','Simulated measurements');
 else
     plot([t(1),t(end)],refline(:,2),'k','LineWidth',2);
     legend('PlaDyPos','Diver estimate','Transect');
 end
 
 setup_plot(hfig);
 
 %% Error plot
 hfig = figure;
 subplot(2,1,1);
 title('Tracking error');
 hold on;
 
 dx = data.TrackPoint.position(1,:)-data.stateHat.position(1,:);
 dy = data.TrackPoint.position(2,:)-data.stateHat.position(2,:);
 
 % Filter the data to show only unique points on a time-plot
 [dxm, ia1, ic1]  = unique(data.usblusbl_nav.point(1,:));
 [dym, ia2, ic2]  = unique(data.usblusbl_nav.point(2,:));
 uidx = union(ia1,ia2);
 dxm = data.usblusbl_nav.point(1,uidx);
 dym = data.usblusbl_nav.point(2,uidx);
 tu = t(uidx);
 
 stairs(t, sqrt(dx.^2 + dy.^2));
 plot(tu, sqrt(dxm.^2 + dym.^2),'rx');
 legend('Estimated error', 'Measured error', 'Location', 'Best');
 
 subplot(2,1,2);
 title('Distance to line');
 hold on;
 
 if isfield(data, 'usblTrackPoint_Gen')
     ltemp = data.usblTrackPoint_Gen.position;
     refline = [ltemp(1,1), ltemp(2,1); ltemp(1,end), ltemp(2,end)];
     dist = track_distance(data.TrackPoint.position(1,:), ...
     data.TrackPoint.position(2,:),refline);
     legend('PlaDyPos','Diver estimate','Simulated measurements');
 else
     dist = track_distance(data.TrackPoint.position(1,:), ...
     data.TrackPoint.position(2,:),refline);
 end 
 

 plot(t, dist);
 legend('Distance');
 
 
 setup_plot(hfig);
 
  %% Tau plot
 hfig = figure;
 title('Tau plot');
 hold on;

 stairs(t, data.tauOut.wrench.force(1,:));
 stairs(t, data.tauOut.wrench.force(2,:),'r');
 axis(gca,[t(1), t(end), -4,4]);
 legend('X', 'Y');
 
 
 setup_plot(hfig);
 
end