function dist = plot_tracks(t, data, refline)
    
    tnames = fields(data);
        
    dist = struct;
    %% Plot separately
    for i=1:length(tnames)
        track = data.(tnames{i});
        tt = t.(tnames{i});
        % Assume that tracks go up-down
        rline = refline;
        if (mod(i,2) == 0)
            rline = [refline(2,:);refline(1,:)];
        end
            
        dist.(tnames{i}) = plot_track(tt, track, rline);
    end   
end