function bag2mat(info, directory)    
    % Parse the info
    bags = load_info(info);
    
    doExtract = 0;

    for i=1:length(bags)
        if ~exist(bags{i}.name,'file')
            disp([bags{i}.name, ' does not exist.']);
            doExtract = 1;
        end
    end

    %% Unzip the data
    if doExtract
        try
            unzip([directory,'/raw_data/filteredbags.zip']);
        catch 
            error('Unable to unzip filteredbags.zip.');
        end
    end

    %% Load the bag files
    data = struct();
    meta = struct();
    tres = struct();
    datares = struct();

    for i=1:length(bags)
        bag = ros.Bag(strcat(bags{i}.name));
        % Export topics and remap them
        [data, meta] = export_bag(bag, ...
            bags{i}.topics.names,1);   
        % Correct missing timestamps for topics or add where non existent
        idx = find(bags{i}.topics.umeta == 1);
        for j=1:length(idx)
            tname = clean_bagname(bags{i}.topics.names{j});
            if isfield(data, tname)
                disp('Adding');
                data.(tname).header.stamp.time = meta.(tname).time.time;
            else
                disp([tname, ' is not a fieldname.']);
            end
        end

        % Resample the data to a single time base
        [tres, datares] = bagstruct_resample(data, ... 
            data.stateHat.header.stamp.time);
       
        elname = strsplit(bags{i}.name,'.');
        elname = elname{1};
       
        matfile = [directory,'/raw_data/', elname, '.mat'];
        save(matfile, 'data', 'meta', 'tres', 'datares');
        disp(['Saved ', matfile]);
        % Remove the file
        % delete(names{i});
    end
end

