function data = bagstruct2ts(data, basetime)
t0 = basetime(1);
tsample = basetime - t0;

topics = fieldnames(data);
baseIsTopic = false;
isTopic = false;

if (isfield(data, 'header'))
  t = data.header.stamp.time - t0;
  baseIsTopic = true;
  isTopic = true;
end

for i=1:length(topics)
   if (~baseIsTopic)
        if isfield(data.(topics{i}),'header')
            t = data.(topics{i}).header.stamp.time - t0;
            isTopic = true;
        else
            isTopic = false;
        end
   end
    
    if (isTopic)
        data.(topics{i}) = recurse_struct(data.(topics{i}), t, tsample, topics{i});
    else
        dips(strcat('Topic ',topics{i}','has no header', ...
            ' and will be skipped'));
        continue;
    end
end

end

function data = recurse_struct(data, t, tsample, name)
    % For empty values return empty
    if isempty(data)
        data = [];
        return;
    end
    
    % For non-structures return resampled time series
    if ~isstruct(data)
        try
            data = timeseries(data,t,'name',name);
            data = resample(data, tsample ,'zoh');
        catch error
            disp(error)
        end
        return;
    end
    
    mf = fields(data);

    for i=1:length(mf)
        if isstruct(data.(mf{i}))
            data.(mf{i}) = recurse_struct(data.(mf{i}), t, tsample, mf{i});
        else
            try
                data.(mf{i}) = timeseries(data.(mf{i}),t, 'name',name);
                data.(mf{i}) = resample(data.(mf{i}), tsample ,'zoh');
            catch error
                disp(strcat('Unable to convert to time series:',mf{i}));
                disp(error)
            end
        end
    end
end