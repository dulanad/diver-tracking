function cname = clean_bagname(name)
%% Hand processing of field names
cname = name;
clear_chars = {'/',' ', ')', '?', ']'};
underscore_chars = {'(', '['};
for j = 1:length(clear_chars)
     cname = strrep(cname,clear_chars{j},''); 
end
for j = 1:length(underscore_chars)
     cname = strrep(cname,underscore_chars{j},'_'); 
end
end