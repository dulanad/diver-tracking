function [data, data_meta] = export_bag(bag, topics, remap)
data = struct();
data_meta = struct();

names = topics;

%% Hand processing of field names
for i=1:length(names)  
    names{i} = clean_bagname(names{i});
end

%% Read data
for i=1:length(names)
    [msgs, meta] = bag.readAll(topics{i});
    if remap == 1
        try
            data.(genvarname(names{i})) = remap_msg(msgs);
        catch error
            disp(strcat('Unable to remap topic', topics{i}));
            disp(error)
            data.(genvarname(names{i})) = [msgs{:}];            
        end
    else
        data.(genvarname(names{i})) = [msgs{:}];
    end
    
    if remap == 1
        try
            data_meta.(genvarname(names{i})) = remap_msg(meta);
        catch error
            disp(strcat('Unable to remap topic', topics{i}));
            disp(error)
            data_meta.(genvarname(names{i})) = [meta{:}];            
        end
    else
        data_meta.(genvarname(names{i})) = [meta{:}];
    end
    
    %data_meta.(genvarname(names{i})) = [meta{:}];
end
end
