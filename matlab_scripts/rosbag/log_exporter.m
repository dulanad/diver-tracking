function varargout = log_exporter(varargin)
% LOG_EXPORTER MATLAB code for log_exporter.fig
%      LOG_EXPORTER, by itself, creates a new LOG_EXPORTER or raises the existing
%      singleton*.
%
%      H = LOG_EXPORTER returns the handle to a new LOG_EXPORTER or the handle to
%      the existing singleton*.
%
%      LOG_EXPORTER('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in LOG_EXPORTER.M with the given input arguments.
%
%      LOG_EXPORTER('Property','Value',...) creates a new LOG_EXPORTER or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before log_exporter_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to log_exporter_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help log_exporter

% Last Modified by GUIDE v2.5 26-May-2014 14:33:17

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @log_exporter_OpeningFcn, ...
                   'gui_OutputFcn',  @log_exporter_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before log_exporter is made visible.
function log_exporter_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to log_exporter (see VARARGIN)

% Choose default command line output for log_exporter
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

if nargin == 3,
    initial_dir = pwd;
elseif nargin > 4
    if strcmpi(varargin{1},'dir')
        if exist(varargin{2},'dir')
            initial_dir = varargin{2};
        else
            errordlg({'Input argument must be a valid',...
                     'folder'},'Input Argument Error!')
            return
        end
    else
        errordlg('Unrecognized input argument',...
                 'Input Argument Error!');
        return;
    end
end
% Populate the listbox
load_listbox(initial_dir,handles)

% UIWAIT makes log_exporter wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = log_exporter_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in FileDialog.
function FileDialog_Callback(hObject, eventdata, handles)
% hObject    handle to FileDialog (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns FileDialog contents as cell array
%        contents{get(hObject,'Value')} returns selected item from FileDialog

disp('Callback');

get(handles.figure1,'SelectionType');
if strcmp(get(handles.figure1,'SelectionType'),'normal')
    set(handles.TopicDialog,'String','',...
	'Value',1)
end
% If double click
if strcmp(get(handles.figure1,'SelectionType'),'open')
    index_selected = get(handles.FileDialog,'Value');
    file_list = get(handles.FileDialog,'String');
    % Item selected in list box
    filename = file_list{index_selected};
    % If folder
    if  handles.is_dir(handles.sorted_index(index_selected))
        cd (filename)
        % Load list box with new folder.
        load_listbox(pwd,handles)
    else
        open_logFile(filename, handles);
    end
end


% --- Executes during object creation, after setting all properties.
function FileDialog_CreateFcn(hObject, eventdata, handles)
% hObject    handle to FileDialog (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in TopicDialog.
function TopicDialog_Callback(hObject, eventdata, handles)
% hObject    handle to TopicDialog (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns TopicDialog contents as cell array
%        contents{get(hObject,'Value')} returns selected item from TopicDialog

if strcmp(get(handles.figure1,'SelectionType'),'normal')
    disp('Normal');
elseif strcmp(get(handles.figure1,'SelectionType'),'open')
    disp('Open');
end

% --- Executes during object creation, after setting all properties.
function TopicDialog_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TopicDialog (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in exportButton.
function exportButton_Callback(hObject, eventdata, handles)
% hObject    handle to exportButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(handles.figure1,'SelectionType'),'normal')
    topics = get(handles.TopicDialog, 'string');
    selected = get(handles.TopicDialog, 'value');    
    disp(topics(selected));
    [data, meta] = export_bag(handles.bag, topics(selected),0);
    assignin('base', 'data', data);
    assignin('base', 'data_meta', meta);
end

% --- Executes on button press in remapButton.
function remapButton_Callback(hObject, eventdata, handles)
% hObject    handle to remapButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(handles.figure1,'SelectionType'),'normal')
    topics = get(handles.TopicDialog, 'string');
    selected = get(handles.TopicDialog, 'value');    
    disp(topics(selected));
    [data, meta] = export_bag(handles.bag, topics(selected),1);
    assignin('base', 'data', data);
    assignin('base', 'data_meta', meta);
end

% --- Populates the listbox.
function load_listbox(dir_path, handles)
cd (dir_path)
dir_struct = dir(dir_path);
[sorted_names,sorted_index] = sortrows({dir_struct.name}');
handles.file_names = sorted_names;
handles.is_dir = [dir_struct.isdir];
handles.sorted_index = sorted_index;
guidata(handles.figure1,handles)
set(handles.FileDialog,'String',handles.file_names,...
	'Value',1)

function load_topicbox(handles)
set(handles.TopicDialog,'String',handles.bag.topics,...
	'Value',1)


% --- Populates the listbox.
function open_logFile(filename, handles)
   disp(strcat('Opening: ',filename));
   
   if get(handles.bagRadio, 'Value')
       handles.bag = ros.Bag(filename);
       guidata(handles.figure1,handles);
       load_topicbox(handles);
   end
    
    
    
