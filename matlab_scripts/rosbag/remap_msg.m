function data = remap_msg(msg)   
    data = struct;
    data = recurse_struct(data, [msg{:}]);
end

function data = recurse_struct(data, m)
    % For empty values return empty
    if isempty(m)
        data = [];
        return;
    end
    
    % For non-structures retain current vector
    if ~isstruct(m)
        data = m;
        return;
    end
    
    mf = fields(m);

    
    for i=1:length(mf)
        if isstruct(m(1).(mf{i}))
            data.(mf{i}) = struct;
            data.(mf{i}) = recurse_struct(data.(mf{i}), [m(:).(mf{i})]);
        else
            data.(mf{i}) = [m(:).(mf{i})];
        end
    end
end
    