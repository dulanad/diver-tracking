function save_plot(filename, directory)
%% Predefined plot characteristics
if isempty(directory)
    directory = '.';
end

epsname = [directory,'/',filename,'.eps'];
pngname = [directory,'/',filename,'.png'];
set(gcf,'PaperPositionMode','auto');
print(epsname, '-depsc2', '-r300');
print(pngname, '-dpng', '-r300');
gscmd = 'gs -o -q -sDEVICE=png256 -dEPSCrop -r300 -o';
system([gscmd,pngname,'  ',epsname])
end

