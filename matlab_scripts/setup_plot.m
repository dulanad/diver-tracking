function setup_plot(hfig)
%% Predefined plot characteristics
[figsize, fontsize, legsize] = plot_info();
set(hfig,'Position',figsize);

hAllAxes = findobj(gcf,'type','axes');
hLeg = findobj(hAllAxes,'tag','legend');
% All axes which are not legends
hAxes = setdiff(hAllAxes,hLeg);
hXLabels = get(hAllAxes,'XLabel');
hYLabels = get(hAllAxes,'YLabel');
hTitles = get(hAllAxes,'Title');

% Set text font size
ht = findobj(gca,'Type','text');
set(ht,'FontSize',fontsize);
set(ht, 'Interpreter', 'tex');

if ~isempty(hAxes)
    for i=1:length(hAxes)
        set(hAxes(i),'FontSize',fontsize);
        box(hAxes(i),'on');
        grid(hAxes(i),'on');
    end
end

if ~isempty(hXLabels)
    if length(hXLabels) == 1
        set(hXLabels,'FontSize',fontsize);
    else
        for i=1:length(hXLabels)
            set(hXLabels{i},'FontSize',fontsize);
        end
    end
end

if ~isempty(hYLabels)
    if length(hYLabels) == 1
        set(hYLabels,'FontSize',fontsize);
    else
      for i=1:length(hYLabels)
        set(hYLabels{i},'FontSize',fontsize);
      end
    end
end

if ~isempty(hTitles)
    if length(hTitles) == 1
        set(hTitles,'FontSize',fontsize);
    else
      for i=1:length(hTitles) 
        set(hTitles{i},'FontSize',fontsize);
      end
    end
end

if ~isempty(hLeg)
    for i=1:length(hLeg)
            set(hLeg(i),'FontSize',legsize);
    end
end

end

