function speed_plot(directory)

figdir = [directory,'figures'];

track005 = 'track3';
track0005 = 'track1';

load([directory, '/data/virtual_data/topside_2014_07_02_15_39_00_filtered_1Hz_VarSpeed_0.05.mat']);
u1ref = data.(track005).usblTrackPoint_Gen.body_velocity(1,:);
u1est = data.(track005).TrackPoint.body_velocity(1,:);
tu1 = t.(track005);
tu1 = tu1 - t.(track005)(1);

load([directory, '/data/virtual_data/topside_2014_07_02_15_39_00_filtered_1Hz_VarSpeed_0.005.mat']);
u2ref = data.(track0005).usblTrackPoint_Gen.body_velocity(1,:);
u2est = data.(track0005).TrackPoint.body_velocity(1,:);
tu2 = t.(track0005);
tu2 = tu2 - t.(track0005)(1) + 1;

 %% Speed plot
 hfig = figure;
 hold on;
 title('Speed plot');

 stairs(tu1, u1ref);
 stairs(tu1, -u1est,'g');
 stairs(tu2, -u2est,'r');
 
 axis(gca,[0,120,0,0.35]);
 set(gca, 'YTick', [0.1,0.2,0.3]);
 xlabel('Time [s]');
 ylabel('Speed [m/s]');
 
 legend('Simulated diver speed', 'Estimated speed (Q = 0.005)', ...
     'Estimated speed (Q = 0.0005)', 'Location', 'NorthEast');
 
 setup_plot(hfig);
 save_plot('speed_plot', figdir);
 
end