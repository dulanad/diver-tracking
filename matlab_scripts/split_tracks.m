function [t,data] = split_tracks(tres, datares, tstart)
    data = struct;
    t = struct;
        
    for i=2:length(tstart)
        idx = find(tres <= tstart(i) & tres >= tstart(i-1));
        name = strcat('track',num2str(i-1));
        data.(name) = recurse_struct(datares, idx);
        t.(name) = tres(idx);
    end
    
end

function data = recurse_struct(data, idx)
    % For empty values return empty
    if isempty(data)
        data = [];
        return;
    end
    
    [n,m] = size(data);
    
    % For non-structures return resampled time series
    if ~isstruct(data)
        try
            if (n == 1)
                data = data(idx);
            else
                data = data(:,idx);
            end
        catch error
            disp(error)
        end
        return;
    end
    
    mf = fields(data);

    for i=1:length(mf)
        if isstruct(data.(mf{i}))
            data.(mf{i}) = recurse_struct(data.(mf{i}), idx);
        else
            try
                [n,m] = size(data.(mf{i}));
                if (n == 1)
                    data.(mf{i}) = data.(mf{i})(idx);
                else
                    data.(mf{i}) = data.(mf{i})(:,idx);
                end 
                
            catch error
                disp(strcat('Unable to index:',mf{i}));
                disp(error)
            end
        end
    end
end