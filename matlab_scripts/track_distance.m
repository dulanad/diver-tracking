function dist = track_distance(x, y, refline)

 v1 = refline(1,:);
 v2 = refline(2,:);
 xy = [x;y];
 
 a =(v2(2) - v1(2))/(v2(1) - v1(1));
 b = 1;
 c = -v1(2)+a*v1(1);
 
 dist = abs(-a*xy(1,:) + b*xy(2,:) + c)/sqrt(a^2+b^2);
end