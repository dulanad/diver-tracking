function table = track_stats(tt, tdata)
tusblm = extract_usbl_abs(tt, tdata);
%% Fitting the line model to estimates
ft = fittype('a*x+b');
x = tdata.TrackPoint.position(2,:)';
y = tdata.TrackPoint.position(1,:)';
[curve, gof] = fit(x, y,ft);
coeff = coeffvalues(curve);
dist = track_distance(x',y',[0,coeff(2); 1 sum(coeff)]);
% Save parameters to table
table.est.fit_mean = mean(dist);
table.est.fit_var = std(dist)^2;
table.est.fit_rmse = gof.rmse;
table.est.data = dist;

%% Fitting the line model to measurements
[curve, gof] = fit(tusblm.y', tusblm.x',ft);
coeff = coeffvalues(curve);
dist = track_distance(tusblm.y, tusblm.x,[0,coeff(2); 1 sum(coeff)]);
% Save parameters to table
table.meas.fit_mean = mean(dist);
table.meas.fit_var = std(dist)^2;
table.meas.fit_rmse = gof.rmse;
table.meas.data = dist;

%% Calculating the mean distance
dxm = tdata.stateHat.position(1,tusblm.uidx) - tusblm.x;
dym = tdata.stateHat.position(2,tusblm.uidx) - tusblm.y;
    
dxe = tdata.stateHat.position(1,:) - tdata.TrackPoint.position(1,:);
dye = tdata.stateHat.position(2,:) - tdata.TrackPoint.position(2,:);

table.meas.track_mean = mean(sqrt(dxm.^2 + dym.^2));
table.meas.data = sqrt(dxm.^2 + dym.^2);
table.est.track_mean = mean(sqrt(dxe.^2 + dye.^2));
end