function table = virt_plot(directory)
% Plot tracking data from a virtual diver experiment.
% Takes part of the pladypos_2014_07_02_18_50_25_filtered log.
figdir = [directory,'figures'];

%% Load and subsample the data from the log
load([directory, '/data/virtual_data/pladypos_2014_07_02_18_50_25_filtered.mat']);
tidx = find((tres >= 463) & (tres <= 1072));
tdata = subsample_struct(datares, tidx);
tt = tres(tidx);
% Get the track statistics
table = track_stats(tt, tdata);
% Prepare data
tusblm = extract_usbl_abs(tt, tdata);
%% North-east plot
hfig = figure;
hold on;
title('E-N plot');

plot(tusblm.x, tusblm.y, 'rx');
plot(tdata.TrackPoint.position(1,:), tdata.TrackPoint.position(2,:),'b','LineWidth',2);

axis(gca,[55,87.65,-8.21,-8.08]);
ylabel('East [m]');
xlabel('North[m]');
  
legend('Virtual diver measurement', 'Virtual diver estimate', ... 
    'Location', 'SouthEast');

setup_plot(hfig);
save_plot('Figure3_1_NE_virtual',figdir);

%% Time plot 1
hfig = figure;
% Normalize time
ptt = tt - tt(1);
tusblm.pt = tusblm.t - tusblm.t(1);

plot_ne_single(ptt,tdata,tusblm);

% Setup plot
subplot(2,1,1);
axis(gca,[0,609,53,89]);
title('North time plot');
xlabel('Time [s]');
ylabel('North [m]');
      
subplot(2,1,2);
axis(gca,[0,609,-9.5,-7.7]);
title('East time plot');
xlabel('Time [s]');
ylabel('East [m]');
hLeg = legend('Virtual diver measured','Virtual diver estimated', ...
        'ASV{\it PlaDyPos} estimated','Location','SouthWest'); 

setup_plot(hfig);
save_plot('Figure3_2_NE_time_virtual',figdir);

%% Time plot 2
hfig = figure;
% Normalize time
tusblm.pt = tusblm.t - tt(1);
ptt = tt - tt(1);

dxm = tdata.stateHat.position(1,tusblm.uidx) - tusblm.x;
dym = tdata.stateHat.position(2,tusblm.uidx) - tusblm.y;
    
dxe = tdata.stateHat.position(1,:) - tdata.TrackPoint.position(1,:);
dye = tdata.stateHat.position(2,:) - tdata.TrackPoint.position(2,:);

hold on;     
d = sqrt(dxm.^2+dym.^2);
md1 = mean(d);
h1 = plot(tusblm.pt, d,'rx'); 
h1b = plot(tusblm.pt, md1*ones(size(tusblm.pt)),'r--'); 
d = sqrt(dxe.^2+dye.^2);
md2 = mean(d);
h2 = stairs(ptt, d,'b','LineWidth',2);  
h2b = plot(ptt, md2*ones(size(ptt)),'b--');
% d = sqrt((vdata.diver_relative_video.n).^2 + (vdata.diver_relative_video.e).^2);
% md3 = mean(d(~isnan(d)));
% h3 = stairs(vdata.tt, d,'g.','LineWidth',2);
% h3b = plot(ptt, md3*ones(size(ptt)),'g--');

% Setup plot
axis(gca,[0,608,0,4]);
set(gca, 'YTick', [0, md1, 1:1:3.5]);
title('Tracking error');
xlabel('Time [s]');
ylabel('Tracking Error [m]');
     
legend([h1,h2], 'Simulated USBL measurement', 'Virtual diver estimate', ...
    'Video validation', 'Location','NorthEast'); 
setup_plot(hfig);
save_plot('Figure3_3_distance_virtual',figdir);

end
