function table = vr_plot(directory)
%% ROV 9.38 test scripts
% Works on 4 tracks from the 9.48 part one log.
% Difference between tangential plane origins during diver and ROV
% tracking
dx = 3.1108;
dy = -14.4480;
figdir = [directory,'figures'];

%% Load the data from the log
load([directory, '/data/rov_data/topside_2014-07-03-09-48-12_part1_filtered.mat']);
load([directory, '/data/rov_data/video_2014-07-03-09-48-12_part1_filtered.mat']);
load([directory, '/data/rov_data/topside_2014-07-03-09-48-12_part1_filtered_imu.mat']);

% Correct video scale (from a 9.3mm estimate to real 8mm based on measuring the ROV across frames)
videodatares.diver_relative_video.x = videodatares.diver_relative_video.x*0.008/0.0093;
videodatares.diver_relative_video.y = videodatares.diver_relative_video.y*0.008/0.0093;

tid1x = find((tres >= 420.3) & (tres <= 720.3));
tid234x = find((tres >= 1185.8) & (tres <= 1891));
tidx = union(tid1x,tid234x);

tdata = subsample_struct(datares, tidx);
tt = tres(tidx);
idata = subsample_struct(imures, tidx);
vdata = subsample_struct(videodatares, tidx);
% Process data
vdata = compensate_video_rpy(vdata, tdata, idata);
vdata = extract_video_meas(tt, vdata);
dxv = vdata.diver_relative_video.n;
dyv = vdata.diver_relative_video.e;

% Get the track statistics
table = track_stats(tt, tdata);
d = sqrt(dxv.^2 + dyv.^2);
table.vid.track_mean = mean(d(~isnan(d)));
table.vid.data = d;
% Prepare data
tusblm = extract_usbl_abs(tt, tdata);
%% North-east plot
hfig = figure;
hold on;
title('N-E plot');

plot(tusblm.y + dy, tusblm.x + dx, 'rx');
plot(tdata.TrackPoint.position(2,:) + dy, tdata.TrackPoint.position(1,:) + dx,'b','LineWidth',2);


axis(gca,[-20,34,18,62]);
xlabel('East [m]');
ylabel('North[m]');
  
legend('ROV position measurement', 'ROV position estimate', ... 
    'Location', 'SouthEast');

setup_plot(hfig);
save_plot('Figure2_1_NE_ROV',figdir);

%% Time plot 1
hfig = figure;
% Extract leg
pdata = subsample_struct(datares, tid234x);
vdata = subsample_struct(videodatares, tid234x);
idata = subsample_struct(imures, tid234x);
ptt = tres(tid234x);
% Process data
vdata = compensate_video_rpy(vdata, pdata, idata);
% Subsample measurements
pusblm = extract_usbl_abs(ptt, pdata);
vdata = extract_video_meas(ptt, vdata);
% Normalize time
pusblm.pt = pusblm.t - ptt(1);
vdata.tt = vdata.tt - ptt(1);
ptt = ptt - ptt(1);
% Plot data
plot_ne_single(ptt,pdata,pusblm);

% Setup plot
subplot(2,1,1);
xlen = 700;
ymin = 16.5;
ymax = 60;
axis(gca,[0,xlen,ymin,ymax]);
title('North plot');
xlabel('Time [s]');
ylabel('North [m]');

subplot(2,1,2);
ymin = -5;
ymax = 65;
axis(gca,[0,xlen,ymin,ymax]);
title('East plot');
xlabel('Time [s]');
ylabel('East [m]');
hlen = legend('ROV measured','ROV estimated', ...
         'ASV{\it PlaDyPos} estimated','Location','SouthEast'); 

setup_plot(hfig);

save_plot('Figure2_2_NE_time_ROV',figdir);

%% Time plot 2
hfig = figure;

dxm = pdata.stateHat.position(1,pusblm.uidx) - pusblm.x;
dym = pdata.stateHat.position(2,pusblm.uidx) - pusblm.y;
    
dxe = pdata.stateHat.position(1,:) -  pdata.TrackPoint.position(1,:);
dye = pdata.stateHat.position(2,:) -  pdata.TrackPoint.position(2,:);

hold on;     
d = sqrt(dxm.^2+dym.^2);
md1 = mean(d);
h1 = plot(pusblm.pt, d,'rx'); 
h1b = plot(pusblm.pt, md1*ones(size(pusblm.pt)),'r--'); 
d = sqrt(dxe.^2+dye.^2);
md2 = mean(d);
h2 = stairs(ptt, d,'b','LineWidth',2);  
h2b = plot(ptt, md2*ones(size(ptt)),'b--');
d = sqrt((vdata.diver_relative_video.n).^2 + (vdata.diver_relative_video.e).^2);
md3 = mean(d(~isnan(d)));
h3 = plot(vdata.tt, d,'g.','LineWidth',2);
h3b = plot(ptt, md3*ones(size(ptt)),'g--');

% Setup plot
axis(gca,[0,xlen,0,4.5]);
set(gca, 'YTick', [0, md2, md1, 2:2:4]);
title('Tracking error');
xlabel('Time [s]');
ylabel('Tracking Error [m]');
     
legend([h1,h2,h3], 'USBL measurement', 'ROV estimate', ...
    'Video validation', 'Location','NorthEast'); 
setup_plot(hfig);
save_plot('Figure2_3_distance_ROV',figdir);
end